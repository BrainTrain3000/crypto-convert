# CryptoConvert (by Brian Stern)

## Features
* "Trades" USD to BTC using current exchange rate
* Validates USD input in real time and on Trade request
* UI is what was requested (I think)

## Instructions
1. First, run `yarn install` or `npm install` to install dependencies.
2. To build for development, run `yarn start:dev` or `npm run start:dev`. This will run `webpack-dev-server` on port 8080. In order for the remote Bitfinex request to work, you'll need to install a browser plugin to allow CORS requests. This is only necessary because the app is running inside `webpack-dev-server`.
3. To build for production, run `yarn build` or `npm run build`. The bundle will be in the `dist` directory.
4. To run tests, run `yarn test` or `npm run test`.

## Code Architecture
You'll need to forgive some serious overengineering here; I'm trying to show off a little.

This is a React/Redux application, with an immutable app state (using [Immer](https://github.com/mweststrate/immer)) and reactive stream-based event handling (using [Redux Observable](https://github.com/redux-observable/redux-observable)). In other words, every UI event (optionally) produces a new app state, and then (optionally) is handled by an Epic (an event stream handler), which (generally) emits a new event.

### Redux Store
There are four key components to how the Redux store handles data:

#### Actions
In Redux, events in the system are modelled as Actions, and they are defined by their ActionCreator functions in `/src/store/action`. No Action is ever created without one of these ActionCreators, and no ActionCreator is defined outside of this folder.  
Each Action is uniquely defined by its Type. An Action may also have a Payload, which is the data needed by the Reducer to produce the new app state, and may also be used by an Epic listening on the Action's Type.

#### Reducers
Reducers take an Action and produce a new app state. With [Immer](https://github.com/mweststrate/immer), I can take a draft state and mutate it. When the reducer function finishes, that draft state will be frozen and applied to the app state tree, causing components dependent on that state to re-render.

#### Epics
Epics listen for specific Action Types, and perform side-effects when triggered. This is how the Exchange Rate remote request is made. It is also used to make an un-asked-for feature: when a user stops typing in the USD trade amount input for 300ms, their input is validated automatically, and any errors present are returned. With this feature, I'm trying to justify using [RxJS](https://github.com/ReactiveX/rxjs) and [Redux Observable](https://github.com/redux-observable/redux-observable) for an application that definitely doesn't need them.

### Selectors
Selectors are pure functions of the Redux store's state. In their simplest form, they simply retrieve a value from the state tree. With the [Reselect](https://github.com/reduxjs/reselect) library, I create automatically-memoized Selector functions that return values derived from the contents of the state tree.

With this, can store the minimum, most direct version of the application state tree in the store, and execute complex transformations on that state to produce values more useful to our UI. This is how the USD->BTC currency conversion is made. Since it's memoized, when state changes that isn't tradeAmountUsd or exchangeRate, our tradeAmountBtc value is not recomputed.

It's a cheap way to optimize code without obfuscating intent.


### React
The React side of the application is much less interesting. Every component is designed to be a pure function of its inputs. Each one (though this need not always be the case) has a ViewModel which binds Redux state elements and ActionCreators to its props. The ViewModels are all the `index.js` files accompanying their JSX components. All state is retrieved using only Selectors; following this pattern makes it easier to trace rendering glitches.

Each component also uses PropTypes validation, which can be useful during development.

I use the [Recompose](https://github.com/acdlite/recompose) library to decorate the App component with the componentDidMount lifecycle event. This library allows me to write pure, functional components without losing out on the benefits extending React.Component.

### SCSS
Not much to say here. I follow the [BEM](http://getbem.com/) design pattern, since it's straightforward and maintainable, and I'm not bothered by its verbosity. SCSS lets me write cleaner, more maintainable stylesheets.

---
That's it! If you're still conscious and reading, you've got a pre-internet attention span, and I don't know how you held onto it.

Looking forward to coming in for my interview!  
--Brian Stern
