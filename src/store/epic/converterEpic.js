import { combineEpics, ofType } from 'redux-observable';

import { Observable, of, throwError } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  pluck,
  switchMap
} from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

import { isNumeric } from 'lib/util';

import ACTION_TYPE from 'store/actionType';

import * as converterAction from 'store/action/converterAction';
import * as converterSelector from 'store/selector/converterSelector';

/** Ensure the tradeAmountUsd and balanceUsd values are valid to make a trade */
export const validateTradeAmountUsd = (tradeAmountUsd, balanceUsd) => {
  if (!isNumeric(tradeAmountUsd) || tradeAmountUsd <= 0) {
    return 'Invalid value for USD amount.';
  }
  const tradeAmountUsdFloat = parseFloat(tradeAmountUsd);
  if (
    parseFloat(tradeAmountUsdFloat) !==
    parseFloat(tradeAmountUsdFloat.toFixed(2))
  ) {
    return 'Invalid value for USD amount.';
  }

  if (tradeAmountUsd * 100 > balanceUsd) {
    return 'Insufficient funds for trade.';
  }
  return null;
};

/** Get the BTC->USD exchange rate from Bitfinex */
const getExchangeRateEpic = action$ =>
  action$.pipe(
    ofType(ACTION_TYPE.GET_EXCHANGE_RATE_REQUEST),
    switchMap(() =>
      ajax.get('https://api.bitfinex.com/v1/pubticker/btcusd').pipe(
        pluck('response', 'last_price'),
        map(converterAction.getExchangeRateSuccess),
        catchError(err => {
          alert('Unable to retrieve exchange rate.');
          return of(converterAction.getExchangeRateFailure(err));
        })
      )
    )
  );

/** Execute a trade, simulating a remote request/response lifecycle by creating an Observable */
const executeTradeEpic = (action$, state$) =>
  action$.pipe(
    ofType(ACTION_TYPE.EXECUTE_TRADE_REQUEST),
    switchMap(() =>
      Observable.create(subscriber => {
        const state = state$.value;
        const tradeAmountBtc = converterSelector.tradeAmountBtcSelector(state);
        const tradeAmountUsd = converterSelector.tradeAmountUsdSelector(state);
        const balanceUsd = converterSelector.balanceUsdSelector(state);

        const error = validateTradeAmountUsd(tradeAmountUsd, balanceUsd);
        if (error !== null) {
          return subscriber.error(new Error(error));
        }
        return subscriber.next(tradeAmountBtc);
      }).pipe(
        map(converterAction.executeTradeSuccess),
        catchError(error => {
          alert(error.message);
          return of(converterAction.executeTradeFailure(error));
        })
      )
    )
  );

/**
 * After 300ms of user inactivity in the tradeAmountUsd input,
 * validate the input value to see if a trade can be made.
 * Return an error message if input is invalid, only if the input is not empty.
 */
const validateTradeAmountUsdEpic = (action$, state$) =>
  action$.pipe(
    ofType(ACTION_TYPE.CHANGE_TRADE_AMOUNT_USD),
    debounceTime(300),
    pluck('payload'),
    map(tradeAmountUsd => {
      if (tradeAmountUsd === '') {
        return null;
      }

      const balanceUsd = converterSelector.balanceUsdSelector(state$.value);

      return validateTradeAmountUsd(tradeAmountUsd, balanceUsd);
    }),
    filter(error => error !== null),
    map(converterAction.tradeAmountUsdError)
  );

export default combineEpics(
  getExchangeRateEpic,
  executeTradeEpic,
  validateTradeAmountUsdEpic
);
