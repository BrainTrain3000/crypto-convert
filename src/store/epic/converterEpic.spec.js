import { validateTradeAmountUsd } from './converterEpic';

describe('converterEpic', () => {
  it('should correctly validate USD trade dollar amounts', () => {
    expect(validateTradeAmountUsd('', 0)).toEqual(
      'Invalid value for USD amount.'
    );
    expect(validateTradeAmountUsd('f', 0)).toEqual(
      'Invalid value for USD amount.'
    );
    expect(validateTradeAmountUsd('-1', 0)).toEqual(
      'Invalid value for USD amount.'
    );
    expect(validateTradeAmountUsd('0', 0)).toEqual(
      'Invalid value for USD amount.'
    );
    expect(validateTradeAmountUsd('1.111', 0)).toEqual(
      'Invalid value for USD amount.'
    );
    expect(validateTradeAmountUsd('1', 0)).toEqual(
      'Insufficient funds for trade.'
    );
    expect(validateTradeAmountUsd('1.0', 100)).toEqual(null);
  });
});
