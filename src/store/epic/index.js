import { combineEpics } from 'redux-observable';

import converter from './converterEpic';

export default combineEpics(converter);
