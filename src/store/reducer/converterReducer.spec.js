import converterReducer, { initialState } from './converterReducer';
import ACTION_TYPE from 'store/actionType';

describe('converterReducer', () => {
  it('should return the initial state', () => {
    expect(converterReducer(undefined, {})).toEqual(initialState);
  });

  it('should set the trade amount in USD and remove errors', () => {
    expect(
      converterReducer(
        { tradeAmountUsd: '', tradeAmountUsdError: 'Oh no' },
        {
          type: ACTION_TYPE.CHANGE_TRADE_AMOUNT_USD,
          payload: '10000'
        }
      )
    ).toEqual({ tradeAmountUsd: '10000', tradeAmountUsdError: '' });
  });

  it('should apply a successful trade', () => {
    expect(
      converterReducer(
        { balanceBtc: 0, balanceUsd: 10000, tradeAmountUsd: '50.00' },
        { type: ACTION_TYPE.EXECUTE_TRADE_SUCCESS, payload: 0.1 }
      )
    ).toEqual({ balanceBtc: 0.1, balanceUsd: 5000, tradeAmountUsd: '' });
  });
});
