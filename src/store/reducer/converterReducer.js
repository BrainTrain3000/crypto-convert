import produce from 'immer';

import ACTION_TYPE from 'store/actionType';

export const initialState = {
  // Numeric dollar currency recorded in cents
  balanceUsd: 15612,
  balanceBtc: 0,
  // Editable currency value stored as string
  tradeAmountUsd: '',
  tradeAmountUsdError: '',
  exchangeRate: null
};

export default produce((draft, action) => {
  switch (action.type) {
    case ACTION_TYPE.CHANGE_TRADE_AMOUNT_USD:
      draft.tradeAmountUsd = action.payload;
      draft.tradeAmountUsdError = '';
      break;

    case ACTION_TYPE.TRADE_AMOUNT_USD_ERROR:
      draft.tradeAmountUsdError = action.payload;
      break;

    case ACTION_TYPE.GET_EXCHANGE_RATE_SUCCESS:
      draft.exchangeRate = action.payload;
      break;

    case ACTION_TYPE.EXECUTE_TRADE_SUCCESS:
      draft.balanceBtc += action.payload;
      draft.balanceUsd -= draft.tradeAmountUsd * 100;
      draft.tradeAmountUsd = '';
      break;
  }
}, initialState);
