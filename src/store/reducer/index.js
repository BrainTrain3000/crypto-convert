import { combineReducers } from 'redux';

import converter from './converterReducer';

export default combineReducers({
  converter
});
