import { createSelector } from 'reselect';

import { isNumeric } from 'lib/util';

export const balanceUsdSelector = state => state.converter.balanceUsd;
export const balanceBtcSelector = state => state.converter.balanceBtc;
export const tradeAmountUsdSelector = state => state.converter.tradeAmountUsd;
export const tradeAmountUsdErrorSelector = state =>
  state.converter.tradeAmountUsdError;
export const exchangeRateSelector = state => state.converter.exchangeRate;

export const tradeAmountBtcSelector = createSelector(
  tradeAmountUsdSelector,
  exchangeRateSelector,
  (tradeAmountUsd, exchangeRate) =>
    isNumeric(tradeAmountUsd) && isNumeric(exchangeRate) && exchangeRate > 0
      ? tradeAmountUsd * (1 / exchangeRate)
      : 0
);
