import * as converterSelector from './converterSelector';
import ACTION_TYPE from 'store/actionType';

describe('converterSelector', () => {
  it('should correctly convert USD to BTC given the exchange rate', () => {
    expect(
      converterSelector.tradeAmountBtcSelector({
        converter: { tradeAmountUsd: '100', exchangeRate: 2 }
      })
    ).toEqual(50);
  });

  it('should default to 0 when there is no exchange rate', () => {
    expect(
      converterSelector.tradeAmountBtcSelector({
        converter: { tradeAmountUsd: '100', exchangeRate: null }
      })
    ).toEqual(0);
  });

  it('should default to 0 when there is an invalid tradeAmountUsd', () => {
    expect(
      converterSelector.tradeAmountBtcSelector({
        converter: { tradeAmountUsd: 'what?', exchangeRate: 2 }
      })
    ).toEqual(0);
  });
});
