import keyMirror from 'fbjs/lib/keyMirror';

module.exports = keyMirror({
  CHANGE_TRADE_AMOUNT_USD: null,
  TRADE_AMOUNT_USD_ERROR: null,

  GET_EXCHANGE_RATE_REQUEST: null,
  GET_EXCHANGE_RATE_SUCCESS: null,
  GET_EXCHANGE_RATE_FAILURE: null,

  EXECUTE_TRADE_REQUEST: null,
  EXECUTE_TRADE_SUCCESS: null,
  EXECUTE_TRADE_FAILURE: null
});
