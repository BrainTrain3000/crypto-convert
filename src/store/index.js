import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import reducer from 'store/reducer';
import epic from 'store/epic';

const epicMiddleware = createEpicMiddleware(epic);

export default () => createStore(reducer, applyMiddleware(epicMiddleware));
