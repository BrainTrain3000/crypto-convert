import * as converterAction from './converterAction';
import ACTION_TYPE from 'store/actionType';

describe('converterAction', () => {
  it('should pull the input value out of an input event', () => {
    expect(
      converterAction.changeTradeAmountUsd({ target: { value: 'hello' } })
    ).toEqual({
      type: ACTION_TYPE.CHANGE_TRADE_AMOUNT_USD,
      payload: 'hello'
    });
  });
});
