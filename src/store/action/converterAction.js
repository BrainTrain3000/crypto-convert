import ACTION_TYPE from 'store/actionType';

/** Change tradeAmountUsd input event */
export const changeTradeAmountUsd = e => ({
  type: ACTION_TYPE.CHANGE_TRADE_AMOUNT_USD,
  payload: e.target.value
});

/** Invalid value for tradeAmountUsd */
export const tradeAmountUsdError = error => ({
  type: ACTION_TYPE.TRADE_AMOUNT_USD_ERROR,
  payload: error
});

/** Exchange rate request */
export const getExchangeRateRequest = () => ({
  type: ACTION_TYPE.GET_EXCHANGE_RATE_REQUEST
});

/** Exchange rate successful response */
export const getExchangeRateSuccess = exchangeRate => ({
  type: ACTION_TYPE.GET_EXCHANGE_RATE_SUCCESS,
  payload: exchangeRate
});

/** Exchange rate error response */
export const getExchangeRateFailure = error => ({
  type: ACTION_TYPE.GET_EXCHANGE_RATE_FAILURE,
  payload: error
});

/**
 * A context-agnostic re-export of the getExchangeRateRequest function
 * (makes refactoring easier if the request lifecycle is changed)
 */
export const getExchangeRate = getExchangeRateRequest;

/** Execute USD->BTC trade request */
export const executeTradeRequest = () => ({
  type: ACTION_TYPE.EXECUTE_TRADE_REQUEST
});

/** Execute USD->BTC trade success response */
export const executeTradeSuccess = amountBtc => ({
  type: ACTION_TYPE.EXECUTE_TRADE_SUCCESS,
  payload: amountBtc
});

/** Execute USD->BTC trade error respones */
export const executeTradeFailure = error => ({
  type: ACTION_TYPE.EXECUTE_TRADE_FAILURE,
  payload: error
});

/**
 * A context-agnostic re-export of the executeTradeRequest function
 * (makes refactoring easier if the request lifecycle is changed)
 */
export const executeTrade = executeTradeRequest;
