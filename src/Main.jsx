import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';

import 'scss/base.scss';

import createStore from 'store';

import App from 'App';

const store = createStore();

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );
};

render(App);
