/** USD trade value component */

import React from 'react';
import PropTypes from 'prop-types';

const TradeUsdView = ({ action, amount, error }) => (
  <div className="trade-view">
    <div className="trade-view__title">Trade</div>
    <div className="trade-view__body">
      <div className="trade-view__label">USD</div>
      <input
        className="trade-view__input"
        value={amount}
        placeholder="Enter your amount"
        onChange={action.changeTradeAmountUsd}
      />
      <div className="trade-view__error">{error}</div>
    </div>
  </div>
);

TradeUsdView.propTypes = {
  action: PropTypes.shape({
    changeTradeAmountUsd: PropTypes.func.isRequired
  }).isRequired,
  amount: PropTypes.string.isRequired,
  error: PropTypes.string
};

export default TradeUsdView;
