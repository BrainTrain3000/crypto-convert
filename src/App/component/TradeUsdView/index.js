/** ViewModel for TradeUsdView */

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { changeTradeAmountUsd } from 'store/action/converterAction';
import {
  tradeAmountUsdSelector,
  tradeAmountUsdErrorSelector
} from 'store/selector/converterSelector';

import TradeUsdView from './TradeUsdView';

const mapStateToProps = state => ({
  amount: tradeAmountUsdSelector(state),
  error: tradeAmountUsdErrorSelector(state)
});

const mapDispatchToProps = dispatch => ({
  action: bindActionCreators({ changeTradeAmountUsd }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TradeUsdView);
