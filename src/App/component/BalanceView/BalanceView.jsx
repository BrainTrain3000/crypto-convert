/** Display for current account balances */

import React from 'react';
import PropTypes from 'prop-types';

import { formatUsd, formatBtc } from 'lib/util';

const BalanceView = ({ balanceUsd, balanceBtc }) => (
  <div className="balance-view">
    <div className="balance-view__title">Account Balance</div>
    <table className="balance-view__table">
      <tbody className="balance-view__table-body">
        <tr className="balance-view__balance">
          <td className="balance-view__label">USD</td>
          <td className="balance-view__value">{formatUsd(balanceUsd)}</td>
        </tr>
        <tr className="balance-view__balance">
          <td className="balance-view__label">BTC</td>
          <td className="balance-view__value">{formatBtc(balanceBtc)}</td>
        </tr>
      </tbody>
    </table>
  </div>
);

BalanceView.propTypes = {
  balanceUsd: PropTypes.number.isRequired,
  balanceBtc: PropTypes.number.isRequired
};

export default BalanceView;
