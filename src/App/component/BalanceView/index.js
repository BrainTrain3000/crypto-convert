/** ViewModel for BalanceView */

import { connect } from 'react-redux';

import {
  balanceUsdSelector,
  balanceBtcSelector
} from 'store/selector/converterSelector';

import BalanceView from './BalanceView';

const mapStateToProps = state => ({
  balanceUsd: balanceUsdSelector(state),
  balanceBtc: balanceBtcSelector(state)
});

export default connect(mapStateToProps)(BalanceView);
