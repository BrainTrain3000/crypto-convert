/** BTC trade value component */

import React from 'react';
import PropTypes from 'prop-types';

import { formatBtc } from 'lib/util';

const TradeBtcView = ({ amount }) => (
  <div className="trade-view">
    <div className="trade-view__title">For</div>
    <div className="trade-view__body">
      <div className="trade-view__label">BTC</div>
      <div className="trade-view__value">{formatBtc(amount)}</div>
    </div>
  </div>
);

TradeBtcView.propTypes = {
  amount: PropTypes.number.isRequired
};

export default TradeBtcView;
