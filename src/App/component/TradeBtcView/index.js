/** ViewModel for TradeBtcView */

import { connect } from 'react-redux';

import { tradeAmountBtcSelector } from 'store/selector/converterSelector';

import TradeBtcView from './TradeBtcView';

const mapStateToProps = state => ({
  amount: tradeAmountBtcSelector(state)
});

export default connect(mapStateToProps)(TradeBtcView);
