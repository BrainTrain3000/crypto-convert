import React from 'react';
import renderer from 'react-test-renderer';

import TradeBtcView from './TradeBtcView';

test('TradeBtcView renders as expected', () => {
  const component = renderer.create(<TradeBtcView amount={0.3} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
