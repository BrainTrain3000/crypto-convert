/** ViewModel for App */

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getExchangeRate, executeTrade } from 'store/action/converterAction';

import { exchangeRateSelector } from 'store/selector/converterSelector';

const mapStateToProps = state => ({
  // Disable "Trade" button if no exchange rate downloaded
  disabledTrading: exchangeRateSelector(state) === null
});

import App from './App';

const mapDispatchToProps = dispatch => ({
  action: bindActionCreators({ getExchangeRate, executeTrade }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
