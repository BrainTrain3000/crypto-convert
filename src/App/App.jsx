/** Top-level application component */

import React from 'react';
import PropTypes from 'prop-types';

import lifecycle from 'recompose/lifecycle';

import BalanceView from './component/BalanceView';
import TradeBtcView from './component/TradeBtcView';
import TradeUsdView from './component/TradeUsdView';

// Download exchange rates when component mounts
const enhance = lifecycle({
  componentDidMount() {
    this.props.action.getExchangeRate();
  }
});

const App = enhance(({ action, disabledTrading }) => (
  <div className="app">
    <div className="app__panel">
      <BalanceView />
      <TradeUsdView />
      <TradeBtcView />
      <button
        className="button"
        onClick={action.executeTrade}
        disabled={disabledTrading}
      >
        Trade
      </button>
    </div>
  </div>
));

App.propTypes = {
  action: PropTypes.shape({
    getExchangeRate: PropTypes.func.isRequired,
    executeTrade: PropTypes.func.isRequired
  }).isRequired,
  disabledTrading: PropTypes.bool.isRequired
};

export default App;
