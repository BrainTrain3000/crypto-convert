export const formatUsd = amountUsd => (amountUsd / 100).toFixed(2);

export const formatBtc = amountBtc => amountBtc.toFixed(8);

export const isNumeric = number =>
  !isNaN(parseFloat(number)) && isFinite(number);
